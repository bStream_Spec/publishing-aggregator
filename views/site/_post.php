<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$sum = [];
?>
<div class="post">



    <div class="article col-lg-12">
        <div class="title"><h3><?= Html::encode($model['title']) ?></h3></div>
        <div class="content"> <?= HtmlPurifier::process($model['content']) ?></div>

        <p><a href="<?= \yii\helpers\Url::to(['site/view', 'id' => $model['id']]) ?>">Подробнее</a></p>
    </div>

</div>