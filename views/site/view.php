<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $article['title'];
?>

<!-- Получение процентов для рейтинга-->
<?php $sum = $rating[0]['count'] + $rating[1]['count'] + $rating[2]['count'] ?>
<?php if ( !empty($rating[0]['count']) && !empty($rating[1]['count']) && !empty($rating[2]['count']) ): ?>

<?php $rate0 = round($rating[0]['count'] * 100 /  $sum); ?>
<?php $rate1 = round($rating[1]['count'] * 100 /  $sum); ?>
<?php $rate2 = round($rating[2]['count'] * 100 /  $sum); ?>
<?php //debug($rating[0]['count']) ?>
<?php endif; ?>
<div class="site-index ">
        <div class="article col-lg-12">
            <div class="title"><h2><?= $article['title']; ?></h2></div>
            <div class="content"> <?= $article['content']; ?></div>

            <?php if( Yii::$app->user->isGuest) : ?>

                <br/>
                <h5>Ввойдите что-бы оценить запись</h5>
            <? endif; ?>
            <?php if(!Yii::$app->user->isGuest): ?>

            <?php if($article['user_id'] != Yii::$app->user->identity->id  ): ?>
                    <!-- Форма рейтингу-->
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($rating_model, 'rate')->radioList([0 => 'Не понравилось', 1 => 'Хорошо', 2 => 'Отлично']) ?>
                <?= Html::submitButton('Оценить', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end(); ?>
            <?php elseif($article['user_id']  == Yii::$app->user->identity->id ): ?>
                    <h5>Автор статьи не может голосовать</h5>
                <?php endif; ?>

                <!-- Оценка рейтинга публикации-->
                <?php if( !empty($rating[0]['count']) && !empty($rating[1]['count']) && !empty($rating[2]['count'])): ?>
                <h5>
                    <?php if( empty($rating[0]['count']) ) echo 0; else echo $rating[0]['count']; ?> пользователей поставило оценку 'Не понравилось',
                    <?php if( empty($rating[1]['count']) ) echo 0; else echo $rating[1]['count']; ?>  пользователя - 'Хорошо',
                    <?php if( empty($rating[2]['count']) ) echo 0; else echo $rating[2]['count']; ?>  пользователей 'Отлично'.
                </h5>
                <?php else: ?>
                    <h5>Рейтинг появится когда все варианты будут иметь минимум один голос.</h5>
                <?php endif; ?>
                <?php if( ($rate0 > $rate1) && ($rate0 > $rate2) ): ?>
                    <h5>Итоговая оценка: 'Не понравилось' (<?= $rate0 ?> %)</h5>
                <?php endif; ?>
                <?php if( ($rate1 > $rate2) && ($rate1 > $rate0) ): ?>
                    <h5>Итоговая оценка: 'Хорошо' (<?= $rate1 ?> %)</h5>
                <?php endif; ?>

                <?php if( ($rate2 > $rate1) && ($rate2 > $rate0) ): ?>
                    <h5>Итоговая оценка: 'Отлично' (<?= $rate2 ?> %)</h5>
                <?php endif; ?>

            <? endif; ?>
        </div>



</div>




