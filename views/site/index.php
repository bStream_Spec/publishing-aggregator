<?php

use yii\widgets\ListView;

$this->title = 'Все публикации';

?>
<div class="site-index container">
    <h1>Все публикации</h1>

        <h5>Сортировать по: <?= $sort->link('data') ?></h5>
        <h5>Сортировать по оценки: <?= $sort->link('rate_bad_count') ?> | <?= $sort->link('rate_good_count') ?> |
            <?= $sort->link('rate_excellent_count') ?> </h5>
        <?php
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_post',
        ]);
        ?>

</div>
