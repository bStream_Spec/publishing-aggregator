<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="addArticle">
    <h2>Добавить публикацию</h2>
    <?php if(Yii::$app->user->isGuest): ?>
        <h3>Ввойдите для добавления публикации.</h3>
    <?php endif; ?>
    <?php if(!Yii::$app->user->isGuest): ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'content')->textarea(['rows' => 5]) ?>

    <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end(); ?>
    <?php endif; ?>
</div>

