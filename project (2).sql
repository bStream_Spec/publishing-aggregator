-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Лис 27 2017 р., 17:57
-- Версія сервера: 5.7.19
-- Версія PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `project`
--

-- --------------------------------------------------------

--
-- Структура таблиці `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rate_bad_count` int(5) UNSIGNED DEFAULT NULL,
  `rate_good_count` int(5) UNSIGNED DEFAULT NULL,
  `rate_excellent_count` int(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `article`
--

INSERT INTO `article` (`id`, `title`, `content`, `user_id`, `data`, `rate_bad_count`, `rate_good_count`, `rate_excellent_count`) VALUES
(31, 'Публикация 1', 'Публикация 1', 3, '2017-11-27 11:14:36', 1, 2, 1),
(32, 'Публикация 2', 'Публикация 2', 3, '2017-11-27 11:14:43', 1, 1, 3),
(33, 'Публикация 3', 'Публикация 3', 3, '2017-11-27 11:14:48', 4, 2, 1),
(34, 'Публикация 4', 'Публикация 4', 60, '2017-11-27 11:17:42', 1, 2, 1),
(35, 'Публикация 5', 'Публикация 5', 60, '2017-11-27 11:17:47', 1, 1, 5),
(36, 'Публикация 6', 'Публикация 6', 60, '2017-11-27 11:19:00', 3, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`, `updated_at`) VALUES
('administrator', 1, 1511475702, 1511475702),
('moderator', 2, 1511475702, 1511475702);

-- --------------------------------------------------------

--
-- Структура таблиці `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, 'Administrator', NULL, NULL, 1511475702, 1511475702),
('moderator', 1, 'Moderator', NULL, NULL, 1511475702, 1511475702),
('rbacManage', 2, 'Management RBAC structure', NULL, NULL, 1511475702, 1511475702),
('userCreate', 2, 'Creating users', NULL, NULL, 1511475702, 1511475702),
('userDelete', 2, 'Deleting users', NULL, NULL, 1511475702, 1511475702),
('userManage', 2, 'Browse list of users', NULL, NULL, 1511475702, 1511475702),
('userPermissions', 2, 'User rights management', NULL, NULL, 1511475702, 1511475702),
('userUpdate', 2, 'Editing users', NULL, NULL, 1511475702, 1511475702),
('userUpdateNoElderRank', 2, 'Editing users with equal or lower rank', 'noElderRank', NULL, 1511475702, 1511475702),
('userView', 2, 'Viewing user information', NULL, NULL, 1511475702, 1511475702);

-- --------------------------------------------------------

--
-- Структура таблиці `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('administrator', 'moderator'),
('administrator', 'rbacManage'),
('administrator', 'userCreate'),
('administrator', 'userDelete'),
('moderator', 'userManage'),
('administrator', 'userPermissions'),
('administrator', 'userUpdate'),
('userUpdateNoElderRank', 'userUpdate'),
('moderator', 'userUpdateNoElderRank'),
('moderator', 'userView');

-- --------------------------------------------------------

--
-- Структура таблиці `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('noElderRank', 'O:34:\"budyaga\\users\\rbac\\NoElderRankRule\":3:{s:4:\"name\";s:11:\"noElderRank\";s:9:\"createdAt\";N;s:9:\"updatedAt\";i:1431880756;}', 1511475702, 1511475702);

-- --------------------------------------------------------

--
-- Структура таблиці `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1511475579),
('m130524_201442_init', 1511475702),
('m171124_085959_create_article_table', 1511514716),
('m171124_090944_create_rating_table', 1511514716);

-- --------------------------------------------------------

--
-- Структура таблиці `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rate` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `rating`
--

INSERT INTO `rating` (`id`, `article_id`, `user_id`, `rate`) VALUES
(1, 31, 60, '1'),
(2, 31, 60, '2'),
(3, 31, 60, '0'),
(4, 31, 60, '1'),
(5, 31, 60, '1'),
(6, 32, 60, '2'),
(7, 32, 60, '2'),
(8, 32, 60, '1'),
(9, 32, 60, '0'),
(10, 32, 60, '2'),
(11, 32, 60, '2'),
(12, 33, 60, '1'),
(13, 33, 60, '1'),
(14, 33, 60, '0'),
(15, 33, 60, '0'),
(16, 33, 60, '0'),
(17, 33, 60, '0'),
(18, 33, 60, '2'),
(19, 33, 60, '2'),
(20, 34, 3, '0'),
(21, 34, 3, '1'),
(22, 34, 3, '2'),
(23, 34, 3, '1'),
(24, 34, 3, '2'),
(25, 35, 3, '1'),
(26, 35, 3, '2'),
(27, 35, 3, '0'),
(28, 35, 3, '2'),
(29, 35, 3, '2'),
(30, 35, 3, '2'),
(31, 35, 3, '2'),
(32, 35, 3, '2'),
(33, 36, 3, '1'),
(34, 36, 3, '0'),
(35, 36, 3, '0'),
(36, 36, 3, '0'),
(37, 36, 3, '2'),
(38, 36, 3, '0');

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` smallint(6) DEFAULT '1',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `email`, `photo`, `sex`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'TZb88qj4IRvzR6sgbjkX8SrStqLMc5Gz', '$2y$13$J8OADnZ8bRBM/460sqZ.R.vev3BrsBOIepLk0QrgBvCSTmcLQwKP2', 'administrator@example.com', NULL, 1, 2, 1511475701, 1511475701),
(2, 'Moderator', 'XjK9i1ExB_ySrPXbx8vKn9C6gpntn2KL', '$2y$13$pfuu5pGJkaYbcnQXZplKfuLykMF0pRs9WOLZ0WC2/otJePud4T8QW', 'moderator@example.com', NULL, 1, 2, 1511475702, 1511475702),
(3, 'Vadim Fyshtey', 'SrNaVdiLZ_dOXcrlFomoJptgjy4CnKlu', '', 'fyshtey@gmail.com', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=200', 1, 2, 1511484003, 1511484003),
(58, 'vadim', 'X1FyUTWA-olaXEMo9wgvArhbMEvWcPTZ', '$2y$13$nUehPsG1Xc3T2pIypsa8peluLmAHOuEJue3GCCRQULAj5LurwV0Vq', 'rusrap88@gmail.com', '', 1, 2, 1511512393, 1511512425),
(59, 'test', 'O35XqIVZu0IxaM-ccrCOtehiIENxc7Pa', '$2y$13$mVCo12DQilEMX/HowWprTewnvPaG/tpEXiQMXTrl1UrWzdvnSv5eK', 'fyshtey66@gmail.com', '', 1, 2, 1511512612, 1511512642),
(60, 'Вадим Фуштей', 'Bz3rPiUYitSdIO9yxzhSzK3iO06p-Roj', '', 'fyshtey@bstream.com.ua', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=200', NULL, 2, 1511518982, 1511518982);

-- --------------------------------------------------------

--
-- Структура таблиці `user_email_confirm_token`
--

CREATE TABLE `user_email_confirm_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `old_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_email_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_email_confirm` smallint(6) DEFAULT '0',
  `new_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_email_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_email_confirm` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `user_oauth_key`
--

CREATE TABLE `user_oauth_key` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `user_oauth_key`
--

INSERT INTO `user_oauth_key` (`id`, `user_id`, `provider_id`, `provider_user_id`) VALUES
(1, 3, 2, '100881376975273284566'),
(2, 60, 2, '100131883913881988781');

-- --------------------------------------------------------

--
-- Структура таблиці `user_password_reset_token`
--

CREATE TABLE `user_password_reset_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_fk` (`user_id`);

--
-- Індекси таблиці `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `auth_item_rule_name_fk` (`rule_name`),
  ADD KEY `auth_item_type_index` (`type`);

--
-- Індекси таблиці `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `auth_item_child_child_fk` (`child`);

--
-- Індекси таблиці `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Індекси таблиці `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Індекси таблиці `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user_email_confirm_token`
--
ALTER TABLE `user_email_confirm_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_email_confirm_token_user_id_fk` (`user_id`);

--
-- Індекси таблиці `user_oauth_key`
--
ALTER TABLE `user_oauth_key`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_oauth_key_user_id_fk` (`user_id`);

--
-- Індекси таблиці `user_password_reset_token`
--
ALTER TABLE `user_password_reset_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_password_reset_token_user_id_fk` (`user_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблиці `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT для таблиці `user_email_confirm_token`
--
ALTER TABLE `user_email_confirm_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `user_oauth_key`
--
ALTER TABLE `user_oauth_key`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `user_password_reset_token`
--
ALTER TABLE `user_password_reset_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_item_name_fk` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_rule_name_fk` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_child_fk` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_parent_fk` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `user_email_confirm_token`
--
ALTER TABLE `user_email_confirm_token`
  ADD CONSTRAINT `user_email_confirm_token_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `user_oauth_key`
--
ALTER TABLE `user_oauth_key`
  ADD CONSTRAINT `user_oauth_key_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `user_password_reset_token`
--
ALTER TABLE `user_password_reset_token`
  ADD CONSTRAINT `user_password_reset_token_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
