<?php

namespace app\models;

use yii\db\ActiveRecord;

class Rating extends ActiveRecord {

    public static function tableName(){
        return 'rating';
    }

    public function rules(){
        return [
            [ ['article_id', 'user_id'], 'safe' ],
            [ ['rate'], 'safe' ],
        ];
    }

    public function attributeLabels(){
        return [
            'rate' => '',
        ];
    }

    public function getArticle(){
        return $this->hasOne(Article::classname(), ['id' => 'article_id']);
    }

    public function getUser(){
        return $this->hasOne(User::classname(), ['id' => 'user_id']);
    }

}