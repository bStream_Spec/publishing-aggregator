<?php

namespace app\models;

use yii\db\ActiveRecord;

class Article extends ActiveRecord{

    public static function tableName(){
        return 'article';
    }

    public function rules(){
        return [
          [ ['title', 'content', 'user_id',], 'required' ],
          [ ['title', 'content'], 'trim' ],
          [ ['data', 'rate_bad_count', 'rate_good_count', 'rate_excellent_count'], 'safe' ],
        ];
    }


    public function attributeLabels(){
       return [
         'title' => 'Название',
         'content' => 'Полный текст',
       ];
    }

    public function getRating(){
        return $this->hasMany(Rating::classname(), ['article_id' => 'id']);
    }




}