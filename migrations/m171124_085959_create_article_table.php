<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article`.
 */
class m171124_085959_create_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'content' => $this->text(),
            'user_id' => $this->integer(11)->unsigned(),
            'data' => $this->dateTime()->defaultValue('CURRENT_TIMESTAMP'),
            'rate_bad_count' => $this->integer(5)->unsigned()->defaultValue(null),
            'rate_good_count' => $this->integer(5)->unsigned()->defaultValue(null),
            'rate_excellent_count' => $this->integer(5)->unsigned()->defaultValue(null),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('article');
    }
}
