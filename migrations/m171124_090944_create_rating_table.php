<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rating`.
 */
class m171124_090944_create_rating_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->defaultValue(null),
            'user_id' => $this->integer()->defaultValue(null),
            'rate' => 'ENUM("0", "1", "2")',

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rating');
    }
}
