<?php

namespace app\controllers;

use app\models\Article;
use app\models\Rating;
use Yii;
use yii\data\Sort;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    public function actionIndex(){

        $dataProvider = new ActiveDataProvider([
            'query' =>  Article::find()->asArray()->with('rating'),
            'pagination' => ['pageSize'=>100]
        ]);

        $sort = new Sort([
            'attributes' => [
                'data' => [
                    'asc' => ['data' => SORT_ASC],
                    'desc' => ['data' => SORT_DESC],
                    'default' => SORT_ASC,
                    'label' => 'Дате',
                ],
                'rate_bad_count' => [
                    'asc' => ['rate_bad_count' => SORT_ASC],
                    'desc' => ['rate_bad_count' => SORT_DESC],
                    'label' => 'Не понравилось',
                ],
                'rate_good_count' => [
                    'asc' => ['rate_good_count' => SORT_ASC],
                    'desc' => ['rate_good_count' => SORT_DESC],
                    'label' => 'Хорошо',
                ],
                'rate_excellent_count' => [
                    'asc' => ['rate_excellent_count' => SORT_ASC],
                    'desc' => ['rate_excellent_count' => SORT_DESC],
                    'label' => 'Отлично',
                ],
            ],
        ]);

        return $this->render('index', compact('dataProvider', 'sort'));
    }

    public function actionView($id){
        $article = Article::findOne($id);
        if(empty($article)){
            throw new \yii\web\HttpException(404, 'Данной страницы не существует.');
        }
        $q = "SELECT rate, count(rate) as `count` FROM `rating` where article_id = $id GROUP by rate";
        $rating = Rating::findBySql($q)->asArray()->all();



        $rating_model = new Rating();
        $rating_model->rate = 1;
        if( $rating_model->load(Yii::$app->request->post())){

            if($rating_model->save()){

                $article->rate_bad_count = $rating[0]['count']; // оценка 'Не понравилось'.
                $article->rate_good_count = $rating[1]['count']; // оценка 'Хорошо'.
                $article->rate_excellent_count = $rating[2]['count']; // оценка 'Отлично'.
                $article->save();

                $rating_model->article_id = $article->id;
                $rating_model->user_id = Yii::$app->user->identity->id;
                $rating_model->save();

                Yii::$app->session->setFlash('success', 'Оценка добавлена');
                return $this->redirect(Yii::$app->request->referrer);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
        }

       return $this->render('view', compact('article', 'rating', 'rating_model'));
    }

    public function actionAdd(){

        $model = new Article();

        if( $model->load(Yii::$app->request->post())){

            $model->user_id = Yii::$app->user->identity->id;
            $model->save();

            if($model->save()){

                Yii::$app->session->setFlash('success', 'Публикацию добавлено');
                return $this->redirect(Yii::$app->request->referrer);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при добавление');
            }
        }

        return $this->render('add', compact('model'));
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
}
